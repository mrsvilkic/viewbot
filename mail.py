import os
import smtplib

import string
import random

from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


class MailSender:
    def __init__(self, username, password, rcpt):
        self.username = username
        self.password = password
        self.smtpserver = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        self.recipient = rcpt

    def set_message(self, subject, body, msg_from=None):
        # self.msg = f"Subject: {in_subject}\n\n{in_plaintext} "
        self.msg = MIMEText(body)
        self.msg["Subject"] = subject
        # if msg_from is not None:
        self.msg["From"] = self.username
        # self.msg.attach(MIMEText(body, "plain"))
        self.msg["To"] = self.recipient

    def set_recipient(self, in_recipient):
        print("ok")

    def connect(self):
        self.smtpserver.login(self.username, self.password)
        self.connected = True

    def disconnect(self):
        self.smtpserver.close()
        self.connected = False

    def send(self, close_connection=True):

        print("Sending to {}".format(self.msg["To"]))
        self.smtpserver.send_message(self.msg, self.username, self.recipient)

        print("Data sent")

        if close_connection:
            self.disconnect()
            print("Connection closed")